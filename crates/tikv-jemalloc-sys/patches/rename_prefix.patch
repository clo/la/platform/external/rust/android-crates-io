diff --git a/src/lib.rs b/src/lib.rs
index 58010af..0f9f795 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -144,7 +144,7 @@ extern "C" {
     ///
     /// If the space cannot be allocated, a null pointer is returned and `errno`
     /// is set to `ENOMEM`.
-    #[cfg_attr(prefixed, link_name = "_rjem_malloc")]
+    #[cfg_attr(prefixed, link_name = "je_malloc")]
     pub fn malloc(size: size_t) -> *mut c_void;
     /// Allocates zero-initialized space for an array of `number` objects, each
     /// of whose size is `size`.
@@ -155,7 +155,7 @@ extern "C" {
     ///
     /// Note: zero-initialized memory need not be the same as the
     /// representation of floating-point zero or a null pointer constant.
-    #[cfg_attr(prefixed, link_name = "_rjem_calloc")]
+    #[cfg_attr(prefixed, link_name = "je_calloc")]
     pub fn calloc(number: size_t, size: size_t) -> *mut c_void;
 
     /// Allocates `size` bytes of memory at an address which is a multiple of
@@ -179,7 +179,7 @@ extern "C" {
     /// The behavior is _undefined_ if:
     ///
     /// * `ptr` is null.
-    #[cfg_attr(prefixed, link_name = "_rjem_posix_memalign")]
+    #[cfg_attr(prefixed, link_name = "je_posix_memalign")]
     pub fn posix_memalign(ptr: *mut *mut c_void, alignment: size_t, size: size_t) -> c_int;
 
     /// Allocates `size` bytes of memory at an address which is a multiple of
@@ -199,7 +199,7 @@ extern "C" {
     ///
     /// * `alignment` is not a power-of-two
     /// * `size` is not an integral multiple of `alignment`
-    #[cfg_attr(prefixed, link_name = "_rjem_aligned_alloc")]
+    #[cfg_attr(prefixed, link_name = "je_aligned_alloc")]
     pub fn aligned_alloc(alignment: size_t, size: size_t) -> *mut c_void;
 
     /// Resizes the previously-allocated memory region referenced by `ptr` to
@@ -238,7 +238,7 @@ extern "C" {
     /// * `ptr` does not match a pointer previously returned by the memory
     ///   allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_realloc")]
+    #[cfg_attr(prefixed, link_name = "je_realloc")]
     pub fn realloc(ptr: *mut c_void, size: size_t) -> *mut c_void;
 
     /// Deallocates previously-allocated memory region referenced by `ptr`.
@@ -254,7 +254,7 @@ extern "C" {
     /// * `ptr` does not match a pointer earlier returned by the memory
     ///   allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_free")]
+    #[cfg_attr(prefixed, link_name = "je_free")]
     pub fn free(ptr: *mut c_void);
 
     /// Allocates at least `size` bytes of memory according to `flags`.
@@ -274,7 +274,7 @@ extern "C" {
     /// # Safety
     ///
     /// The behavior is _undefined_ if `size == 0`.
-    #[cfg_attr(prefixed, link_name = "_rjem_mallocx")]
+    #[cfg_attr(prefixed, link_name = "je_mallocx")]
     pub fn mallocx(size: size_t, flags: c_int) -> *mut c_void;
 
     /// Resizes the previously-allocated memory region referenced by `ptr` to be
@@ -307,7 +307,7 @@ extern "C" {
     /// * `ptr` does not match a pointer earlier returned by
     ///   the memory allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_rallocx")]
+    #[cfg_attr(prefixed, link_name = "je_rallocx")]
     pub fn rallocx(ptr: *mut c_void, size: size_t, flags: c_int) -> *mut c_void;
 
     /// Resizes the previously-allocated memory region referenced by `ptr` _in
@@ -348,7 +348,7 @@ extern "C" {
     /// * `ptr` does not match a pointer earlier returned by the memory
     ///   allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_xallocx")]
+    #[cfg_attr(prefixed, link_name = "je_xallocx")]
     pub fn xallocx(ptr: *mut c_void, size: size_t, extra: size_t, flags: c_int) -> size_t;
 
     /// Returns the real size of the previously-allocated memory region
@@ -363,7 +363,7 @@ extern "C" {
     /// * `ptr` does not match a pointer earlier returned by the memory
     ///   allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_sallocx")]
+    #[cfg_attr(prefixed, link_name = "je_sallocx")]
     pub fn sallocx(ptr: *const c_void, flags: c_int) -> size_t;
 
     /// Deallocates previously-allocated memory region referenced by `ptr`.
@@ -378,7 +378,7 @@ extern "C" {
     ///   allocation functions of this crate, or
     /// * `ptr` is null, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_dallocx")]
+    #[cfg_attr(prefixed, link_name = "je_dallocx")]
     pub fn dallocx(ptr: *mut c_void, flags: c_int);
 
     /// Deallocates previously-allocated memory region referenced by `ptr` with
@@ -398,7 +398,7 @@ extern "C" {
     ///   allocation functions of this crate, or
     /// * `ptr` is null, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_sdallocx")]
+    #[cfg_attr(prefixed, link_name = "je_sdallocx")]
     pub fn sdallocx(ptr: *mut c_void, size: size_t, flags: c_int);
 
     /// Returns the real size of the allocation that would result from a
@@ -412,7 +412,7 @@ extern "C" {
     /// # Safety
     ///
     /// The behavior is _undefined_ if `size == 0`.
-    #[cfg_attr(prefixed, link_name = "_rjem_nallocx")]
+    #[cfg_attr(prefixed, link_name = "je_nallocx")]
     pub fn nallocx(size: size_t, flags: c_int) -> size_t;
 
     /// Returns the real size of the previously-allocated memory region
@@ -437,7 +437,7 @@ extern "C" {
     /// * `ptr` does not match a pointer earlier returned by the memory
     ///   allocation functions of this crate, or
     /// * the memory region referenced by `ptr` has been deallocated.
-    #[cfg_attr(prefixed, link_name = "_rjem_malloc_usable_size")]
+    #[cfg_attr(prefixed, link_name = "je_malloc_usable_size")]
     pub fn malloc_usable_size(ptr: *const c_void) -> size_t;
 
     /// General interface for introspecting the memory allocator, as well as
@@ -470,7 +470,7 @@ extern "C" {
     /// directly related to `mallctl` read/write processing.
     ///
     /// [jemalloc_mallctl]: http://jemalloc.net/jemalloc.3.html#mallctl_namespace
-    #[cfg_attr(prefixed, link_name = "_rjem_mallctl")]
+    #[cfg_attr(prefixed, link_name = "je_mallctl")]
     pub fn mallctl(
         name: *const c_char,
         oldp: *mut c_void,
@@ -492,11 +492,11 @@ extern "C" {
     /// a complete MIB. For name components that are integers (e.g. the 2 in
     /// arenas.bin.2.size), the corresponding MIB component will always be that
     /// integer.
-    #[cfg_attr(prefixed, link_name = "_rjem_mallctlnametomib")]
+    #[cfg_attr(prefixed, link_name = "je_mallctlnametomib")]
     pub fn mallctlnametomib(name: *const c_char, mibp: *mut size_t, miblenp: *mut size_t) -> c_int;
 
     /// Like [`mallctl`] but taking a `mib` as input instead of a name.
-    #[cfg_attr(prefixed, link_name = "_rjem_mallctlbymib")]
+    #[cfg_attr(prefixed, link_name = "je_mallctlbymib")]
     pub fn mallctlbymib(
         mib: *const size_t,
         miblen: size_t,
@@ -533,7 +533,7 @@ extern "C" {
     /// Note that thread caching may prevent some statistics from being
     /// completely up to date, since extra locking would be required to merge
     /// counters that track thread cache operations.
-    #[cfg_attr(prefixed, link_name = "_rjem_malloc_stats_print")]
+    #[cfg_attr(prefixed, link_name = "je_malloc_stats_print")]
     pub fn malloc_stats_print(
         write_cb: Option<unsafe extern "C" fn(*mut c_void, *const c_char)>,
         cbopaque: *mut c_void,
@@ -550,7 +550,7 @@ extern "C" {
     ///
     /// Please note that doing anything which tries to allocate memory in this
     /// function is likely to result in a crash or deadlock.
-    #[cfg_attr(prefixed, link_name = "_rjem_malloc_message")]
+    #[cfg_attr(prefixed, link_name = "je_malloc_message")]
     pub static mut malloc_message:
         Option<unsafe extern "C" fn(cbopaque: *mut c_void, s: *const c_char)>;
 
@@ -580,7 +580,7 @@ extern "C" {
     /// Some options have boolean values (`true`/`false`), others have integer
     /// values (base `8`, `10`, or `16`, depending on prefix), and yet others
     /// have raw string values.
-    #[cfg_attr(prefixed, link_name = "_rjem_malloc_conf")]
+    #[cfg_attr(prefixed, link_name = "je_malloc_conf")]
     pub static malloc_conf: Option<&'static c_char>;
 }
 
