# This file is generated by cargo_embargo.
# Do not modify this file after the LOCAL_DIR line
# because the changes will be overridden on upgrade.
# Content before the first line starting with LOCAL_DIR is preserved.

LOCAL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
MODULE_CRATE_NAME := libc
MODULE_RUST_CRATE_TYPES := rlib
MODULE_SRCS := $(LOCAL_DIR)/src/lib.rs
MODULE_ADD_IMPLICIT_DEPS := false
MODULE_RUST_EDITION := 2015
MODULE_RUSTFLAGS += \
	--cfg 'freebsd11' \
	--cfg 'libc_align' \
	--cfg 'libc_cfg_target_vendor' \
	--cfg 'libc_const_extern_fn' \
	--cfg 'libc_const_size_of' \
	--cfg 'libc_core_cvoid' \
	--cfg 'libc_int128' \
	--cfg 'libc_long_array' \
	--cfg 'libc_non_exhaustive' \
	--cfg 'libc_packedN' \
	--cfg 'libc_priv_mod_use' \
	--cfg 'libc_ptr_addr_of' \
	--cfg 'libc_underscore_const_names' \
	--cfg 'libc_union' \
	-A unknown-lints \

MODULE_LIBRARY_DEPS := \
	trusty/user/base/lib/libcompiler_builtins-rust \
	trusty/user/base/lib/libcore-rust

ifeq ($(call TOBOOL,$(TRUSTY_USERSPACE)),true)

MODULE_RUSTFLAGS += \
	--cfg 'feature="trusty_sys"' \

MODULE_LIBRARY_DEPS += \
	trusty/user/base/lib/libc-trusty \
	trusty/user/base/lib/trusty-sys \

endif

include make/library.mk
