# This file is generated by cargo_embargo.
# Do not modify this file after the LOCAL_DIR line
# because the changes will be overridden on upgrade.
# Content before the first line starting with LOCAL_DIR is preserved.

LOCAL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
MODULE_CRATE_NAME := bitflags
MODULE_RUST_CRATE_TYPES := rlib
MODULE_SRCS := $(LOCAL_DIR)/src/lib.rs
MODULE_RUST_EDITION := 2021
MODULE_LIBRARY_DEPS := \
	trusty/user/base/lib/libcompiler_builtins-rust \
	trusty/user/base/lib/libcore-rust \
	$(call FIND_CRATE,serde) \

include make/library.mk
